Ниже будет представлен краткий набор правил, которым необходимо следовать при написании `e2e` тестов в данном репозитории.

Для начала, хочется сказать, что `smarty-e2e` совместно со `smarty-docker` может стать довольно мощным иструментом не только для тестирования,
но и для отладки (дебага) различных кейсов.
Вся особенность заключается в том, что в рамках данных сервисов мы можем (с разной степенью трудности) проследить
весь путь от запроса к `API` до изменений в базе и ответа конечному пользователю.
Причём сделать это, не поднимая весь набор сервисов локально на вашем компьютере.
А это - и есть ключ к нахождению проблем и багов в `smarty`-сервисах, следовательно, приведению нашей инфраструктуры в стабильное состояние.

Краткое описание работы сервиса `smarty-docker` в интеграции с `smarty-e2e` для понимания процесса: 
//тут подумать насколько подробно (Рассказать кратко об образах, контейнерах, интерфейсах)

Собственно говоря, для того, чтобы пользоваться сервисом было удобно, нужно соблюдать несколько правил:

## before all hook: failed
Довольно распространённая проблема, причиной которой часто указывают `smarty-docker`. На данный момент, сервис довольно стабильный, поэтому
если в вашем тесте часто проскакивает такая ошибка - это, в большинстве случаев, ваша вина. Почему, спросите вы? 
Всё очень просто, начните руководствоваться правилом: падать должен не `before`, а конкретный блок `it`.

Рассмотрим на примере неправильно написанного `before`:

```javascript
before(async () => {
    await inf.start();

    agent = await inf.getInterface('http');
    db = await inf.getInterface('db');
    oplog = await inf.getInterface('oplog');

    waitDelivery = waitForEvents('op', oplog, {
        filter: data => (data['ns'] === 'crm.delivery')
    });

    res = await agent.post('/api/v1/signup').send({
        email: 'delivery@init.com',
        password: '123456'
    });

    waitTimerInformationLetter = waitForEvents('op', oplog, {
        filter: data => (data['ns'] === 'crm.timer-events' && data['o']['_id'] === `informationLetter_${db_user._id}`)
    });

    waitTimerNotActivityLongTimeLetter = waitForEvents('op', oplog, {
        filter: data => (data['ns'] === 'crm.timer-events' && data['o']['_id'] === `notActivityLongTimeLetter_${db_user._id}`)
    });

    waitTimerCheckActivity = waitForEvents('op', oplog, {
        filter: data => (data['ns'] === 'crm.timer-events' && data['o']['_id'] === `checkActivity_${db_user._id}`)
    });

    await waitDelivery;
    delivery = await db.collection('delivery').find({ user: ObjectId(db_user._id) })
        .limit(1).next();

    await waitTimerInformationLetter;
    informationLetter = await db.collection('timer-events').find({ _id: `informationLetter_${db_user._id}` })
        .limit(1).next();
});
```

Данный блок `before` - рекордсмен по падениям с вышеописанной ошибкой.
Вся проблема в том, что вы даже не сможете понять, какого именно действия вы не дождались без изменения кода или его дебага.
Ведь внутри этого блока - целых 3 объекта с возможными состояниями (событие может совершитсья/не совершиться).

Описание самих событий, а именно:
```javascript
waitTimerCheckActivity = waitForEvents('op', oplog, {
        filter: data => (data['ns'] === 'crm.timer-events' && data['o']['_id'] === `checkActivity_${db_user._id}`)
});
```
Добавлять само описание в `before` - хороший тон, а вот дожидаться их совершения - плохой.
    
Таким образом, данный блок должен был выглядеть так:
    
```javascript
before(async () => {
    await inf.start();
    agent = await inf.getInterface('http');
    db = await inf.getInterface('db');
    oplog = await inf.getInterface('oplog');
    waitDelivery = waitForEvents('op', oplog, {
        filter: data => (data['ns'] === 'crm.delivery')
    });
    res = await agent.post('/api/v1/signup').send({
        email: 'delivery@init.com',
        password: '123456'
    });
    waitTimerInformationLetter = waitForEvents('op', oplog, {
        filter: data => (data['ns'] === 'crm.timer-events' && data['o']['_id'] === `informationLetter_${db_user._id}`)
    });
    waitTimerNotActivityLongTimeLetter = waitForEvents('op', oplog, {
        filter: data => (data['ns'] === 'crm.timer-events' && data['o']['_id'] === `notActivityLongTimeLetter_${db_user._id}`)
    });
    waitTimerCheckActivity = waitForEvents('op', oplog, {
        filter: data => (data['ns'] === 'crm.timer-events' && data['o']['_id'] === `checkActivity_${db_user._id}`)
    });
});

it('description for waitDelivery action assertion', async () => {
    await waitDelivery;
    delivery = await db.collection('delivery').find({ user: ObjectId(db_user._id) })
        .limit(1).next();
    
    delivery.should.be.deliverySchema();
    delivery.lastQuotas.should.have.properties({
        contacts: 0,
        filesSize: 0
    });
    delivery.activityCount.should.be.equal(1);
});

it('description for waitTimerInformationLetter action assertion`, async () => {
    await waitTimerInformationLetter;
    informationLetter = await db.collection('timer-events').find({ _id: `informationLetter_${db_user._id}` })
        .limit(1).next();
        
    JSON.parse(informationLetter.payload).should.have.properties({
            id: db_user._id,
            action: "informationLetter",
            day: 5
        });
    (informationLetter.time > (registrationDate + 5 * day24) && informationLetter.time < (registrationDate + 6 * day24))
        .should.be.true();
});
```
Заметим, что проверка осуществляется запросом к базе данных.
Кроме этого, проверять появление объекта в базе, в данном случае, можно по пришедшему в `waitForEvents` объекту;
    
```javascript
it('description for waitTimerInformationLetter action assertion`, async () => {
    const incomingMessage = await waitTimerInformationLetter;
        
    incomingMessage[0]['o'].should.have.properties({
            id: db_user._id,
            action: "informationLetter",
            day: 5
        });
});
```
    
С точки зрения валидации оба подхода эквивалентны.
        
    
## Custom messages on waitingForEvents.
    
Другая самая распространённая ошибка -

`Error: Timeout expired for waiting events ("op")`

Её тоже следует избегать, ведь она вызывает только непонимание и путаницу. 
Для того, чтобы сделать это, нужно просто дописать сообщение, которое должно пояснять, выполнение какого именно действия ожидалось.

Вместо:
    
```javascript
const waitEmail = waitForEvents('op', oplog, {
    filter: data => (data['ns'] === 'crm.emailTest' && data['o']['subject'] === 'birthday')
});
```
    
Добавить сообщение:
    
```javascript
const waitEmail = waitForEvents('op', oplog, {
    filter: data => (data['ns'] === 'crm.emailTest' && data['o']['subject'] === 'birthday'),
    message: 'Email with birthday subject wasn\'t sent'
});
```    
    
Таким образом, вместо получения непонятной ошибки, мы вполне сможем сделать первичную оценку того, что именно не сработало.
    
## Один it - один assertion.
Основа алгоритма данного правила заложена в самом названии.
Правильное описание блока `it` - ключ к пониманию смысла теста без прочтения его внутреннего кода.

Давайте разберём пару примеров:

```javascript
it('Params should be ok`, async () => {
...
});
```
Вот наглядная демонстрация `bad style`.
"Параметры должны быть в норме" - сама фраза являет собой обобщённую цель всех тестов, а не конкретного теста.

Пример правильно оформленных `it`-блоков:

```javascript
describe('DB', () => {
    it('Should create user', async () => {
       ...
    });

    it('Should create two root goals', async() => {
       ...
    });

    it('Should create first group of contacts', async() => {
        ...
    });
});
```

Такой блок очень легко понять и интерпретировать в действия, даже не зная внутреннго кода проверок.

- проверка на создание объекта пользователя в базе
- проверка работы middleware, которая должна создать 2 цели
- проверка работы middleware, которая создаёт первичную группу

В данном случае не стоит графоманствовать и в отдельных блоках проверять параметры объекта в базе - валидацию можно выполнить непосредственно в данном блоке.

## О filter, waitForEvents, oplog

Зачастую проверки внутри наших тестов строятся на ожидании результата нашего `http` запроса, с последующих появлением/изменением объектов в БД и ответом по сокетам.
Для того, чтобы проверить выполнение этих действий, мы используем хелпер `waitForEvents` в который можно передать интерфейс (`oplog`, `io` etc.)

Для понимания работы `oplog` советую ознокомиться со статьей https://www.compose.com/articles/the-mongodb-oplog-and-node-js/ 
Достаточно будет прочтения пункта `Down in the oplog`.

Знания внутренних полей полезно, ведь когда мы ждём конкретного события нужно и правильно описать кондиции ожидания (`filter`).

Рассмотрим следующий пример:
```javascript
oplogEmailPromise = waitForEvents('op', oplog, {
    filter: data => (data['ns'] === 'crm.emailTest')
});

****

const emailMessage = await oplogEmailPromise;
emailMessage.subject.should.be.equal('shareInvite');
```
    
Краткая суть теста, откуда взят кусок кода:
Расшариваем коллекции одного юезра другому юзеру. Соответственно ждём, что `smarty-email` отправит письмо о расшаривании.

В таком контексте описанный выше код выглядит вполне логичным, но мы забываем, что перед тем, как расшарить, мы зарегистрировали пользователя, могли сделать другие действия, которые приводят
к отправки сообщений на очередь `email_Sender` => вызывает работу сервиса `smarty-email`, все эти процессы происходят асинхронно.

Поэтому старайтесь максимально конкретезировать событие, которого нужно ождидать.
    
 ```javascript
oplogEmailPromise = waitForEvents('op', oplog, {
    filter: data => (data['ns'] === 'crm.emailTest' && data['o']['subject'] === 'shareInvite')
});
```
    
Продолжая рассуждения на тему хелпера `waitForEvents`, вы должны знать, что по умолчанию, он дожидается `pass` одного события, что может вызывать появления неправильного кода в тестах, например:
    
```javascript
oplogRightsPromise = waitForEvents('op', oplog, {
    filter: data => (data['o'] && data['ns']  == 'crm.rights'),
});
```
Но при расшаривании должны создаться несколько объектов в коллеции `rights`, т.е. данная проверка являлась бы не до конца верной (если только отдельно по базе не проверять их количество).

В любом случае, рекомендую пользоваться опцией `count` для наглядного понимания количества событий.
    
```javascript
oplogRightsPromise = waitForEvents('op', oplog, {
    filter: data => (data['o'] && data['ns']  == 'crm.rights'),
    count: COLLECTIONS_FOR_SHARE_NUMBER
});
```
    
    